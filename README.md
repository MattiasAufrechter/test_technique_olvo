# test technique Olvo


## Mon rendu

Bonjour, 

Voici mon rendu de test technique. Pour les fonctionnalités, j'ai fait la partie 1, 2 et 3 (la partie réparation qui s'ajoute aux pannes).

Pour faire cette application, j'ai créé 3 tables distinctes :
- Une ```velos```
- Une ```pannes```
- Une ```reparations```

Que j'ai relié par des models : 

- AddPanneIdToReparations :

Dans ce model, j'ai fait en sorte que ```reparations``` soit dépendant de ```pannes```.

- AddVeloIdToPannes :

Dans ce model, j'ai fait en sorte que ```pannes``` soit dépendant de ```velos```.

Ensuite, j'ai mis 3 controllers qui permettent de prendre toutes les actions CRUD sur un élément d'une table.

Pour les fichiers html, j'ai fait un 'form' par fichier que j'ai réutilisé pour la même fichier (pour les action : update, create)

Pour finir avec la partie fonctionnalités, la route.rb, j'ai fait en sorte que les vélos soit la homepage et j'ai fait des routes via ```resources```.


Ensuite, pour la partie CSS, j'ai repris le fichier application.css, qui se situe dans app>assets>stylesheets.


Si vous avez des questions en plus a me poser, n'hésitez pas à me contacter.

### Getting started

```
ruby bin/rails db:migrate
ruby bin/rails server
```
