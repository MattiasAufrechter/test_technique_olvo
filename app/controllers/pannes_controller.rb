class PannesController < ApplicationController
  def index
    @pannes = Panne.all
  end

  def show
    @panne = Panne.find(params[:id])
    @reparation = Reparation.new
    @reparations = @panne.reparations
  end

  def new
    @panne = Panne.new
  end

  def edit
    @panne = Panne.find(params[:id])
  end

  def update
    @panne = Panne.find(params[:id])

    respond_to do |format|
      if @panne.update(panne_params)
        format.html { redirect_to panne_url(@panne), notice: "La panne a bien été modifiée" }
      else
        render :edit
      end
    end
  end

  def create
    @panne = Panne.new(panne_params)

    if @panne.save
      respond_to do |format|
        format.html { redirect_to panne_url(@panne), notice: "La panne a bien été créée" }
      end
    else
      render :new
    end
  end

  def destroy
    @panne = Panne.find(params[:id])
    @panne.destroy
    redirect_to pannes_path, status: :see_other, notice: "La panne a bien été supprimée"
  end

  private

  def panne_params
    params.require(:panne).permit(:categorie_de_panne, :explication_de_la_panne, :velo_id)
  end
end
