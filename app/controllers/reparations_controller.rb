class ReparationsController < ApplicationController
  def create
    @panne = Panne.find(params[:panne_id])
    @reparation = @panne.reparations.create(reparation_params)

    if @reparation.save
      respond_to do |format|
        format.html { redirect_to panne_url(@panne), notice: "La reparation a bien été créée" }
      end
    end
  end

  def destroy
    @panne = Panne.find(params[:panne_id])
    @reparation = Reparation.find(params[:id])
    @reparation.destroy
    redirect_to @panne, status: :see_other, notice: "La reparation a bien été supprimée"
  end

  private

  def reparation_params
    params.require(:reparation).permit(:reparation_faite, :maniere_de_reparer, :materiel_utiliser)
  end
end
