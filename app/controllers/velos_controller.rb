class VelosController < ApplicationController
  def index
    @velos = Velo.includes(:pannes).all
  end

  def show
    @velo = Velo.find(params[:id])
  end

  def new
    @velo = Velo.new
  end

  def edit
    @velo = Velo.find(params[:id])
  end

  def update
    @velo = Velo.find(params[:id])

    respond_to do |format|
      if @velo.update(velo_params)
        format.html { redirect_to velo_url(@velo), notice: "Le vélo a bien été modifier" }
      else
        render :edit
      end
    end
  end

  def create
    @velo = Velo.new(velo_params)

    if @velo.save
      respond_to do |format|
        format.html { redirect_to velo_url(@velo), notice: "Le vélo a bien été crée" }
      end
    else
      render :new
    end
  end

  def destroy
    @velo = Velo.find(params[:id])
    @velo.destroy
    redirect_to velos_path, status: :see_other, notice: "Le vélo a bien été supprimé"
  end

  private

  def velo_params
    params.require(:velo).permit(:nom_de_velo, :type_de_velo)
  end
end
