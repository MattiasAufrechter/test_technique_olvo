class Panne < ApplicationRecord
    belongs_to :velo

    has_many :reparations, dependent: :destroy

    validates :explication_de_la_panne, :presence => true
end
