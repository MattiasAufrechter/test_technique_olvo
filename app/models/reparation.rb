class Reparation < ApplicationRecord
    belongs_to :panne

    validates :reparation_faite, :maniere_de_reparer ,:materiel_utiliser, :presence => true
end
