class Velo < ApplicationRecord
    #Lorsqu'un vélo est supprimé, la panne sur le vélo l'est aussi
    has_many :pannes, dependent: :destroy

    validates :nom_de_velo, :presence => true, :uniqueness => true
end
