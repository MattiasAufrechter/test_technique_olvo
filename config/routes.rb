Rails.application.routes.draw do
  root 'velos#index'
  
  
  resources :pannes do
    resources :reparations
  end
  resources :velos
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
