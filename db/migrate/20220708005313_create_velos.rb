class CreateVelos < ActiveRecord::Migration[7.0]
  def change
    create_table :velos do |t|
      t.string :nom_de_velo
      t.string :type_de_velo

      t.timestamps
    end
  end
end
