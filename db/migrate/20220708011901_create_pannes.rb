class CreatePannes < ActiveRecord::Migration[7.0]
  def change
    create_table :pannes do |t|
      t.string :categorie_de_panne
      t.text :explication_de_la_panne

      t.timestamps
    end
  end
end
