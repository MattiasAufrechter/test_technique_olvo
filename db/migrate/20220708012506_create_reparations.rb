class CreateReparations < ActiveRecord::Migration[7.0]
  def change
    create_table :reparations do |t|
      t.string :reparation_faite
      t.text :maniere_de_reparer
      t.text :materiel_utiliser

      t.timestamps
    end
  end
end
