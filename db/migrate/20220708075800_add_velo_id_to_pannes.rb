class AddVeloIdToPannes < ActiveRecord::Migration[7.0]
  def change
    add_column :pannes, :velo_id, :integer
  end
end
