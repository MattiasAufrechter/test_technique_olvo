class AddPanneIdToReparations < ActiveRecord::Migration[7.0]
  def change
    add_reference :reparations, :panne, null: false, foreign_key: true
  end
end
