# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_07_08_154456) do
  create_table "pannes", force: :cascade do |t|
    t.string "categorie_de_panne"
    t.text "explication_de_la_panne"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "velo_id"
  end

  create_table "reparations", force: :cascade do |t|
    t.string "reparation_faite"
    t.text "maniere_de_reparer"
    t.text "materiel_utiliser"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "panne_id", null: false
    t.index ["panne_id"], name: "index_reparations_on_panne_id"
  end

  create_table "velos", force: :cascade do |t|
    t.string "nom_de_velo"
    t.string "type_de_velo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "reparations", "pannes"
end
