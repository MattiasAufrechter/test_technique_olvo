require "test_helper"

class PannesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get pannes_index_url
    assert_response :success
  end

  test "should get show" do
    get pannes_show_url
    assert_response :success
  end

  test "should get new" do
    get pannes_new_url
    assert_response :success
  end

  test "should get edit" do
    get pannes_edit_url
    assert_response :success
  end
end
