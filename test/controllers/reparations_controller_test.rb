require "test_helper"

class ReparationsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get reparations_index_url
    assert_response :success
  end

  test "should get show" do
    get reparations_show_url
    assert_response :success
  end

  test "should get new" do
    get reparations_new_url
    assert_response :success
  end

  test "should get edit" do
    get reparations_edit_url
    assert_response :success
  end
end
