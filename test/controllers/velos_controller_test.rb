require "test_helper"

class VelosControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get velos_index_url
    assert_response :success
  end

  test "should get show" do
    get velos_show_url
    assert_response :success
  end

  test "should get new" do
    get velos_new_url
    assert_response :success
  end

  test "should get edit" do
    get velos_edit_url
    assert_response :success
  end
end
